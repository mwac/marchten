def workspaceFolderName = "${WORKSPACE_NAME}"
def projectFolderName = "${PROJECT_NAME}"
def samplefolderName = projectFolderName + "/SampleFolder"
def samplefolder = folder(samplefolderName) { displayName('Folder created by Jenkins') }

//Jobs
def Job1 = freeStyleJob(samplefolderName + "/FirstJob")
def Job2 = freeStyleJob(samplefolderName + "/SecondJob")

//Pipeline
def samplepipeline = buildPipelineView(samplefolderName + "/Sample-Pipeline-Demo")

samplepipeline.with{
	title('Pipeline Demo')
	displayedBuilds(5)
	selectedJob(samplefolderName + "/FirstJob")
	showPipelineParameters()
	refreshFrequency(5)
}

//Job Configuration
Job1.with{
	scm{
		git{
			remote{
				url('https://mwac:whilmz19@bitbucket.org/mwac/marchten.git')
				credentials("adop-jenkins-master")
			}
			branch("*/master")
		}
	}
	steps{
	shell('''#!/bin/sh
	ls -lart''')
	}
	publishers{
		downstreamParameterized{
		trigger(samplefolderName + "/SecondJob") {
			condition("SUCCESS")
			parameters{
				predefinedProp("CUSTOM_WORKSPACE",'$WORKSPACE')
				}
			}
		}
	}
}
Job2.with{
	parameters{
		stringParam("CUSTOM_WORKSPACE","","")
	}
	steps{
	shell('''
	#!/bin/sh
	ls -lart $CUSTOM_WORKSPACE
	''')
	}
}